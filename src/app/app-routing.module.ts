import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'list',
    loadChildren: './list/list.module#ListPageModule'
  },
  {
    path: 'clue-list',
    loadChildren: './clue-list/clue-list.module#ClueListPageModule'
  },  { path: 'scenario-list', loadChildren: './scenario-list/scenario-list.module#ScenarioListPageModule' },
  { path: 'game-list', loadChildren: './game-list/game-list.module#GameListPageModule' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
