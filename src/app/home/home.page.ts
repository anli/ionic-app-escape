import { AlertController } from '@ionic/angular';
import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  constructor(public alertCtrl: AlertController) {}

  async onPlay() {
    const alert = await this.alertCtrl.create({
      header: 'Starting Game',
      inputs: [
        {
          name: 'code',
          type: 'text',
          placeholder: 'Enter Game Code'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {}
        },
        {
          text: 'Ok',
          handler: () => {}
        }
      ]
    });
    await alert.present();
  }
}
