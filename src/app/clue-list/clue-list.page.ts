import { Component, OnInit } from '@angular/core';
import { of, Observable, BehaviorSubject } from 'rxjs';
import * as Chance from 'chance';
import * as R from 'ramda';

const chance = new Chance();
const mockData = (): Clue[] => {
  const items = [];
  items.length = 50;
  return R.map(() => ({
    image: 'http://placeimg.com/50/50/' + chance.natural({ min: 1, max: 200 }),
    name: chance.word(),
    description: chance.paragraph(),
    unlockCode: 'U' + chance.natural({ min: 1, max: 200 }),
    archiveCode: 'A' + chance.natural({ min: 1, max: 200 })
  }))(items);
};

interface Clue {
  name: string;
  description?: string;
  unlockCode: string;
  archiveCode?: string;
  image: string;
}
@Component({
  selector: 'app-clue-list',
  templateUrl: './clue-list.page.html',
  styleUrls: ['./clue-list.page.scss']
})
export class ClueListPage implements OnInit {
  clues$: Observable<Clue[]>;
  totalCount$: BehaviorSubject<number> = new BehaviorSubject(undefined);

  constructor() {}

  ngOnInit() {
    this.totalCount$.next(200);
    this.clues$ = of(mockData());
  }
}
