import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScenarioListPage } from './scenario-list.page';
import { By } from '@angular/platform-browser';

describe('ScenarioListPage', () => {
  let component: ScenarioListPage;
  let fixture: ComponentFixture<ScenarioListPage>;
  let de: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ScenarioListPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScenarioListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a title Scenarios', () => {
    de = fixture.debugElement.query(By.css('ion-title'));
    const text = de.nativeElement.textContent.trim();
    expect(text).toBe('Scenarios');
  });

  it('should have a menu button', () => {
    de = fixture.debugElement.query(By.css('ion-menu-button'));
    expect(de).toBeTruthy();
  });

  it('should have a search button', () => {
    de = fixture.debugElement.query(By.css('ion-button.search-button'));
    expect(de).toBeTruthy();
  });

  it('should have a list header showing 50 of 200 records', () => {
    de = fixture.debugElement.query(By.css('ion-list-header > ion-label'));
    const text = de.nativeElement.textContent.trim();
    expect(text).toBe('Showing 50 of 200 records');
  });

  it('should have a list of 50 items', () => {
    const app = fixture.nativeElement;
    const items = app.querySelectorAll('ion-list > ion-item');
    expect(items.length).toEqual(50);
  });

  it('should have a create button', () => {
    de = fixture.debugElement.query(By.css('ion-fab-button'));
    expect(de).toBeTruthy();
  });
});
