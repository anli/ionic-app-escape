import { Component, OnInit } from '@angular/core';
import { of, Observable, BehaviorSubject } from 'rxjs';
import * as Chance from 'chance';
import * as R from 'ramda';

const chance = new Chance();
const mockData = (): Scenario[] => {
  const items = [];
  items.length = 50;
  return R.map(() => ({
    image: 'http://placeimg.com/50/50/' + chance.natural({ min: 1, max: 200 }),
    name: chance.word()
  }))(items);
};

interface Scenario {
  name: string;
  description?: string;
  unlockCode: string;
  archiveCode?: string;
  image: string;
}
@Component({
  selector: 'app-scenario-list',
  templateUrl: './scenario-list.page.html',
  styleUrls: ['./scenario-list.page.scss']
})
export class ScenarioListPage implements OnInit {
  scenarios$: Observable<Scenario[]>;
  totalCount$: BehaviorSubject<number> = new BehaviorSubject(undefined);

  constructor() {}

  ngOnInit() {
    this.totalCount$.next(200);
    this.scenarios$ = of(mockData());
  }
}
