import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { TestBed, async } from '@angular/core/testing';

import { Platform, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { RouterTestingModule } from '@angular/router/testing';

import { AppComponent } from './app.component';
import { By } from '@angular/platform-browser';

describe('AppComponent', () => {
  let statusBarSpy,
    splashScreenSpy,
    platformReadySpy,
    platformSpy,
    alertCtrlSpy;
  let de: DebugElement;

  beforeEach(async(() => {
    statusBarSpy = jasmine.createSpyObj('StatusBar', [
      'styleLightContent',
      'backgroundColorByHexString'
    ]);
    splashScreenSpy = jasmine.createSpyObj('SplashScreen', ['hide']);
    platformReadySpy = Promise.resolve();
    platformSpy = jasmine.createSpyObj('Platform', { ready: platformReadySpy });
    alertCtrlSpy = jasmine.createSpyObj('AlertController', ['create']);

    TestBed.configureTestingModule({
      declarations: [AppComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: StatusBar, useValue: statusBarSpy },
        { provide: SplashScreen, useValue: splashScreenSpy },
        { provide: Platform, useValue: platformSpy },
        { provide: AlertController, useValue: alertCtrlSpy }
      ],
      imports: [RouterTestingModule.withRoutes([])]
    }).compileComponents();
  }));

  it('should create the app', async () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should initialize the app', async () => {
    TestBed.createComponent(AppComponent);
    expect(platformSpy.ready).toHaveBeenCalled();
    await platformReadySpy;
    expect(statusBarSpy.styleLightContent).toHaveBeenCalled();
    expect(statusBarSpy.backgroundColorByHexString).toHaveBeenCalledWith(
      '#3171e0'
    );
    expect(splashScreenSpy.hide).toHaveBeenCalled();
  });

  it('should have menu labels', async () => {
    const fixture = await TestBed.createComponent(AppComponent);
    await fixture.detectChanges();
    const app = fixture.nativeElement;
    const menuItems = app.querySelectorAll('ion-label');
    expect(menuItems.length).toEqual(6);
    expect(menuItems[0].textContent).toContain('Home');
    expect(menuItems[1].textContent).toContain('Start Game');
    expect(menuItems[2].textContent).toContain('Clues');
    expect(menuItems[3].textContent).toContain('Scenarios');
    expect(menuItems[4].textContent).toContain('Games');
    expect(menuItems[5].textContent).toContain('Logout');
  });

  it('should have urls', async () => {
    const fixture = await TestBed.createComponent(AppComponent);
    await fixture.detectChanges();
    const app = fixture.nativeElement;
    const menuItems = app.querySelectorAll('ion-item');
    expect(menuItems.length).toEqual(6);
    expect(menuItems[0].getAttribute('ng-reflect-router-link')).toEqual(
      '/home'
    );
    expect(menuItems[2].getAttribute('ng-reflect-router-link')).toEqual(
      '/clue-list'
    );
    expect(menuItems[3].getAttribute('ng-reflect-router-link')).toEqual(
      '/scenario-list'
    );
    expect(menuItems[4].getAttribute('ng-reflect-router-link')).toEqual(
      '/game-list'
    );
    expect(menuItems[5].getAttribute('ng-reflect-router-link')).toEqual(
      '/logout'
    );
  });

  it('should be able to open the prompt to start a game', async () => {
    const fixture = await TestBed.createComponent(AppComponent);
    await fixture.detectChanges();
    de = fixture.debugElement.query(By.css('ion-item.game-start'));

    de.triggerEventHandler('click', null);
    expect(alertCtrlSpy.create).toHaveBeenCalled();
  });
});
