import { Component, OnInit } from '@angular/core';
import { of, Observable, BehaviorSubject } from 'rxjs';
import * as Chance from 'chance';
import * as R from 'ramda';

const chance = new Chance();
const mockData = (): Game[] => {
  const items = [];
  items.length = 50;
  return R.map(() => ({
    image: 'http://placeimg.com/50/50/' + chance.natural({ min: 1, max: 200 }),
    name: chance.word(),
    code: 'G' + chance.natural({ min: 1, max: 200 }),
    scenarioName: chance.word()
  }))(items);
};

interface Game {
  name: string;
  code: string;
  image: string;
  scenarioName: any;
}
@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.page.html',
  styleUrls: ['./game-list.page.scss']
})
export class GameListPage implements OnInit {
  games$: Observable<Game[]>;
  totalCount$: BehaviorSubject<number> = new BehaviorSubject(undefined);

  constructor() {}

  ngOnInit() {
    this.totalCount$.next(200);
    this.games$ = of(mockData());
  }
}
